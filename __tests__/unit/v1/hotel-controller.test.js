const RoomService = require("../../../src/services/v1/HotelService");
const moment = require("moment");

describe("Search validation", () => {
  it("should be truthy when the period is valid", () => {
    const checkin = moment();
    const checkout = moment(checkin).add(2, "day");

    expect(
      RoomService.validatePeriod(checkin.toISOString(), checkout.toISOString())
    ).toBe(true);
  });
  it("should be falsy when the period is invalid", () => {
    const checkin = moment();
    const checkout = moment(checkin).add(-2, "day");

    expect(
      RoomService.validatePeriod(checkin.toISOString(), checkout.toISOString())
    ).toBe(false);
  });
});
