const express = require("express");

class AppController {
  constructor() {
    this.express = express();

    this.middlewares();
    this.routes();
  }

  routes() {
    this.express.use(require("./routes/router"));
  }

  middlewares() {
    this.express.use(express.json());
  }
}

module.exports = new AppController().express;
