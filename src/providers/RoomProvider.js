const BrowserService = require("../services/BrowserService");
const moment = require("moment");

class RoomProvider {
  constructor() {
    this.browser = null;
  }

  async roomsByPeriodRange(start, end, pageSpecs, retry = true) {
    let page;
    try {
      page = await this.getPage(this.getURL(start, end, pageSpecs), pageSpecs);
    } catch (e) {
      if (retry) {
        // retry one time
        this.roomsByPeriodRange(start, end, pageSpecs, false)
      }
      this.closeBrowser();
      throw new Error("Page load took too long");
    }

    // evaluates if there are available plans
    const roomsAvailable = await page.$$eval(
      `.${pageSpecs.roomPlans.selector}`,
      (divs) => divs.length
    );

    if (!roomsAvailable) {
      return [];
    }

    // select the rooms list, process each room and returns their data
    const roomsHandle = await page.$eval(
      pageSpecs.selectorToWaitFor,
      (element, pageSpecs) => {
        // get all rooms
        const rooms = element.getElementsByClassName(pageSpecs.room.selector);
        const values = [];
        for (let i = 0; i < rooms.length; i++) {
          // get room's name
          const nameElement = rooms[i].getElementsByClassName(
            pageSpecs.roomName.selector
          )[0];
          // get room's description
          const descriptionElement = rooms[i].getElementsByClassName(
            pageSpecs.roomDescription.selector
          )[0];
          // get room's image]
          let image = "";
          if (pageSpecs.roomImage.type === "class") {
            image = rooms[i].getElementsByClassName(
              pageSpecs.roomImage.selector
            )[0].src;
          } else if (pageSpecs.roomImage.type === "tag") {
            image = rooms[i].getElementsByTagName(pageSpecs.roomImage.selector)[
              pageSpecs.roomImage.index
            ].src;
          }
          // instantiate room data
          const roomData = {
            name: nameElement
              ? nameElement.textContent.replace(/[\n\t]/g, "")
              : "",
            description: descriptionElement
              ? descriptionElement.textContent.replace(/[\n\t]/g, "")
              : "",
            price: 0,
            image,
            plans: [],
          };
          // get room's plans
          const ratePlans = rooms[i].getElementsByClassName(
            pageSpecs.roomPlans.selector
          );
          // if there is a plan then gets the price and add the room in the returned array
          if (ratePlans && ratePlans.length) {
            for (
              let ratePlanIndex = 0;
              ratePlanIndex < ratePlans.length;
              ratePlanIndex++
            ) {
              const ratePolicyElement = ratePlans
                .item(ratePlanIndex)
                .getElementsByClassName(pageSpecs.roomPlanPolicy.selector)[0];
              const ratePriceElement = ratePlans
                .item(ratePlanIndex)
                .getElementsByClassName(pageSpecs.roomPlanPrice.selector)[0];
              roomData.plans.push({
                ratePolicy: ratePolicyElement
                  ? ratePolicyElement.textContent
                  : "",
                ratePrice: ratePriceElement
                  ? ratePriceElement.textContent.replace(/[\n\t]/g, "")
                  : "",
              });
            }
            roomData.price = roomData.plans[0].ratePrice || 0;
            values.push(roomData);
          }
        }
        return values;
      },
      pageSpecs
    );

    this.closeBrowser();
    return roomsHandle;
  }

  async getPage(goToURL, pageSpecs) {
    console.log(goToURL)
    this.browser = await BrowserService.getBrowser();
    // create a page
    const page = await this.browser.newPage();
    // go to url
    await page.goto(goToURL, {
      waitUntil: "networkidle0",
    });
    // and waits until the rooms list to appear
    await page.waitForSelector(pageSpecs.selectorToWaitFor, {
      visible: true,
    });
    return page;
  }

  async closeBrowser() {
    // BrowserService.closeBrowser(this.browser);
  }

  getURL(start, end, pageSpecs) {
    return pageSpecs.url
      .replace("${start}", moment(start).format(pageSpecs.periodDateFormat))
      .replace("${end}", moment(end).format(pageSpecs.periodDateFormat))
      .replace("${startParam}", pageSpecs.startParam)
      .replace("${endParam}", pageSpecs.endParam);
  }
}

module.exports = RoomProvider;
