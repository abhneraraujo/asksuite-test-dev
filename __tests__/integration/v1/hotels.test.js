const request = require("supertest");
const app = require("../../../src/app");
const mockedPuppetter = require('puppeteer');

describe("Search", () => {
  it('should fetch rooms', async () => {
    const response = await request(app)
      .post("/v1/hotels/search")
      .send({ checkin: "2021-07-01", checkout: "2021-07-03" });

    expect(response.status).toBe(200);
    expect(response.body).toHaveProperty('rooms');
    expect(response.body).toHaveProperty('message');
    expect(response.body).toHaveProperty('length');
    expect(response.body.length).toBe(3);
  });

  it('should fetch rooms', async () => {
    const response = await request(app)
      .post("/v1/hotels/search")
      .send({ checkin: "2021-07-01", checkout: "2021-07-03" });

    expect(response.status).toBe(200);
    expect(response.body).toHaveProperty('rooms');
    expect(response.body).toHaveProperty('message');
    expect(response.body).toHaveProperty('length');
    expect(response.body.length).toBe(3);
  });

  it('should search the page with checkin and checkout dates', async () => {
    const response = await request(app)
      .post("/v1/hotels/search")
      .send({ checkin: "2021-07-01", checkout: "2021-07-03" });

    expect(mockedPuppetter.goto).toHaveBeenCalled();
    expect(mockedPuppetter.goto).toHaveBeenCalledWith(
      "https://book.omnibees.com/hotelresults?CheckIn=01072021&CheckOut=03072021&Code=AMIGODODANIEL&NRooms=1&_askSI=d34b1c89-78d2-45f3-81ac-4af2c3edb220&ad=2&ag=-&c=2983&ch=0&diff=false&group_code=&lang=pt-BR&loyality_card=&utm_source=asksuite&q=5462",
      { "waitUntil": "networkidle0" });
  })
});
