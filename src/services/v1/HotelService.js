const moment = require("moment");
const RoomProvider = require("../../providers/RoomProvider");
const hotelsPageSelectors = require("../../../assets/hotels_page_selectors.json");

class HotelService {
  constructor() {
    this.roomProvider = new RoomProvider();
  }

  search(checkin, checkout) {
    return new Promise(async (resolve, reject) => {
      if (!this.validatePeriod(checkin, checkout)) {
        reject({ message: "Invalid period", status: 409 });
      }

      try {
        const rooms = await this.roomProvider.roomsByPeriodRange(
          checkin,
          checkout,
          hotelsPageSelectors[0]
        );
        resolve({
          rooms,
          length: rooms.length,
          message: rooms.length
            ? "success"
            : "No rooms available for the current filters",
        });
      } catch (error) {
        reject({
          message: error.message,
          status: 409,
        });
      }
    });
  }

  searchAll(checkin, checkout) {
    return new Promise(async (resolve, reject) => {
      if (!this.validatePeriod(checkin, checkout)) {
        reject({ message: "Invalid period", status: 409 });
      }

      const roomsByHotel = [];
      try {
        for (let pageSpec of hotelsPageSelectors) {
          const rooms = await this.getRoomsByHotel(checkin, checkout, pageSpec);
          roomsByHotel.push({
            hotelName: pageSpec.hotel,
            rooms,
          });
        }
      } catch (e) {}
      resolve({
        hotels: roomsByHotel,
        length: roomsByHotel.length,
        message: roomsByHotel.length
          ? "success"
          : "No rooms available for the current filters",
      });
    });
  }

  async getRoomsByHotel(checkin, checkout, pageSpec) {
    try {
      return await this.roomProvider.roomsByPeriodRange(
        checkin,
        checkout,
        pageSpec
      );
    } catch (e) {
      return [];
    }
  }

  validatePeriod(checkin, checkout) {
    try {
      return moment(checkout).diff(checkin, "day") >= 0;
    } catch (e) {
      return false;
    }
  }
}

module.exports = new HotelService();
