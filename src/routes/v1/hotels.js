const routes = require("express").Router();

const HotelController = require("../../controllers/v1/HotelController");

routes.route("/search").post(HotelController.search);

routes.route("/search-all").post(HotelController.searchAll);

module.exports = routes;
