const mockedPuppetter = require('puppeteer');
const BrowserService = require('../../../src/services/BrowserService');

describe("BrowserService", () => {
  it('should launch browser', async () => {
    await BrowserService.getBrowser();

    expect(mockedPuppetter.launch).toHaveBeenCalled();
  });

  it('should close the browser', async () => {
    await BrowserService.closeBrowser(mockedPuppetter);

    expect(mockedPuppetter.close).toHaveBeenCalled();
  });


  it('should not close the browser when it is empty', async () => {
    await BrowserService.closeBrowser(null);

    expect(mockedPuppetter.close).toHaveBeenCalledTimes(0);
  });
})