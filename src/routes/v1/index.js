const router = require("express").Router();

const hotels = require("./hotels");

router.use("/hotels", hotels);

module.exports = router;
