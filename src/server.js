require("dotenv").config();

const express = require("./app");

const port = process.env.PORT;

const server = express.listen(port || 8080, () => {
  console.log(`Listening on port ${port}`);
});
server.setTimeout(60000);
