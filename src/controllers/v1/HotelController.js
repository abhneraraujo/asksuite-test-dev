const hotelService = require("../../services/v1/HotelService");

class HotelController {
  async search(req, res) {
    try {
      const response = await hotelService.search(
        req.body.checkin,
        req.body.checkout
      );
      return res.json(response);
    } catch (error) {
      return res.status(error.status).json({ message: error.message });
    }
  }

  async searchAll(req, res) {
    try {
      const response = await hotelService.searchAll(
        req.body.checkin,
        req.body.checkout
      );
      return res.json(response);
    } catch (error) {
      return res.status(error.status).json({ message: error.message });
    }
  }
}

module.exports = new HotelController();
