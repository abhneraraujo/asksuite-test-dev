module.exports = {
  launch: jest.fn(() => {
    return new Promise((resolve, reject) => resolve(require('puppeteer')));
  }),
  close: jest.fn(() => {
    return new Promise((resolve, reject) => resolve(require('puppeteer')));
  }),
  newPage: jest.fn(() => {
    return new Promise((resolve, reject) => resolve(require('puppeteer')));
  }),
  goto: jest.fn(() => {
    return new Promise((resolve, reject) => resolve(require('puppeteer')));
  }),
  waitForSelector: jest.fn(() => {
    return new Promise((resolve, reject) => resolve(require('puppeteer')));
  }),
  $$eval: jest.fn(() => {
    return new Promise((resolve, reject) => resolve(1));
  }),
  $eval: jest.fn(() => {
    return new Promise((resolve, reject) => resolve([1,2,3]));
  }),
}